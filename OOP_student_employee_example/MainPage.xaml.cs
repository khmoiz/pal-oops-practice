﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace OOP_student_employee_example
{
    /// <summary>
    /// A simple app that allows users to enter information for a record and view the instructions for each type of person record.
    /// There are 3 types of records: Person - Student - Employee
    /// Person is the Base Class which contains the data Name,Email,ID & Department
    /// Student & Employee are derived from Person class and contain messages that pertain to them.
    /// 
    /// The app also contains error checking.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();


        }

        private void EnterRecord(object sender, RoutedEventArgs e)
        {
            string fname = personName.Text.Trim();
            string emailID = personEmail.Text.Trim();
            string departmentID = (string)departments.SelectedItem; //If no option selected from comboBox then departmentID is null
            string sID = personID.Text.Trim();

            // If either of the fields are empty OR if a value has not been selected from the comboBox then display an error message
            if (fname.Length != 0 && emailID.Length != 0 && sID.Length != 0 && departmentID !=null)
            {
                try
                {
                    //Check to see if the value entered by user is an Integer, if not then exception -
                    //is caught and respective message is displayed to the user.
                    int pid = int.Parse(sID);


                    //Check to see which type of Person is selected in order to create the correct Object
                    if ((bool)student.IsChecked)
                    {
                        Student stu = new Student(fname, emailID, departmentID, pid);
                        display.Text = stu.DescribeMe();
                    }
                    else if ((bool)employee.IsChecked)
                    {
                        Employee stu = new Employee(fname, emailID, departmentID, pid);
                        display.Text = stu.DescribeMe();

                    }
                    else if ((bool)person.IsChecked)
                    {
                        Person stu = new Person(fname, emailID, departmentID, pid);
                        display.Text = stu.DescribeMe();
                    }
                }
                catch (Exception caught)
                {

                    display.Text = "Error:\n- Please enter a number value for ID!";

                }
            }
            else
            {
                string message = "Error:\n- Data Missing. Please enter information into all fields!";

                //If the combo box has not been selected then we add an extra message
                if(departmentID == null)
                {
                    message = message + "\n- Please make sure to select a department from the list of options!";
                }

                display.Text = message;
            }

        }
    }

    public class Person
    {
        string _name, _email, _department;
        int _id;

        public Person(string name, string email, string department, int id)
        {
            _name = name;
            _email = email;
            _department = department;
            _id = id;
        }

        public virtual String DescribeMe()
        {

            return $"Name: {_name} \nEmail: {_email}\nID: {_id}\nDepartment: {_department}";
        }
    }

    public class Student : Person
    {
        public Student(string name, string email, string department, int id) : base(name, email, department, id)
        {

        }

        public override string DescribeMe()
        {
            return base.DescribeMe() + "\n Please report to the residence to get your room number and keys!";
        }

    }

    public class Employee : Person
    {
        public Employee(string name, string email, string department, int id) : base(name, email, department, id)
        {

        }

        public override string DescribeMe()
        {
            return base.DescribeMe() + "\n Please report to the Manager's office in B-Wing to recieve your training handouts!";
        }

    }
}
